# $Id$
#
# SYNOPSIS
#
#   AX_RAGEL([ACTION-IF-FOUND[, [ACTION-IF-NOT-FOUND]])
#
# DESCRIPTION
#
#   Test for Ragel.
#
# LAST MODIFICATION
#
#   2021-01-05
#
# LICENSE
#
#  Written by Greg Wickham <greg.wickham@gmail.com>
#  and placed under Public Domain
#

AC_DEFUN([AX_RAGEL], [
    AC_CHECK_PROG(RAGEL,ragel,'')
    if [[ -n "${RAGEL}" ]]; then
        RAGEL=$( which ragel )
    else
        AC_MSG_ERROR("'ragel' required for compliation")
    fi
])
